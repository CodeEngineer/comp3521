#pragma once

#include <memory>

namespace lab7
{
	template<typename T>
	class Node;

	template<typename T>
	class DoublyLinkedList
	{
	public:
		DoublyLinkedList();
		void Insert(std::unique_ptr<T> data);
		void Insert(std::unique_ptr<T> data, unsigned int index);
		bool Delete(const T& data);
		int Find(const T& data) const;
		bool Search(const T& data) const;
		std::shared_ptr<Node<T>> operator[](unsigned int index) const;
		unsigned int GetLength() const;

	private:
		std::shared_ptr<Node<T>> mRoot;
		unsigned int mCount;
		// Add any other private variables/methods here

	};

	// Add implementations of public methods here
	template<typename T>
	DoublyLinkedList<T>::DoublyLinkedList()
		: mCount(0)
	{
	}

	template<typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data)
	{
		std::unique_ptr<Node<T>> node = std::make_unique<Node<T>>(std::move(data));
		if (mRoot == nullptr)
		{
			mRoot = std::move(node);
		}
		else
		{
			std::shared_ptr<Node<T>> tempNode = mRoot;
			while (tempNode->Next != nullptr)
			{
				tempNode = tempNode->Next;
			}
			tempNode->Next = std::move(node);
			tempNode->Next->Previous = tempNode;
		}
		mCount++;
	}

	template<typename T>
	void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data, unsigned int index)
	{
		if (mCount > index)
		{
			if (mCount == 0 && index == 0 || mCount == index)
			{
				Insert(std::move(data));
			}
			else if (mCount > 0 && index == 0)
			{
				std::shared_ptr<Node<T>> node = std::make_shared<Node<T>>(std::move(data));
				node->Next = mRoot;
				mRoot->Previous = node;
				mRoot = node;
			}
			else
			{
				std::shared_ptr<Node<T>> node = std::make_shared<Node<T>>(std::move(data));
				std::shared_ptr<Node<T>> tempNode = mRoot;
				while (index > 0)
				{
					tempNode = tempNode->Next;
					index--;
				}
				// previous node
				std::shared_ptr<Node<T>> lockedPrevNode = tempNode->Previous.lock();
				lockedPrevNode->Next = node;
				node->Previous = lockedPrevNode;
				node->Next = tempNode;
				tempNode->Previous = node;
			}
			mCount++;
		}
		else
		{
			Insert(std::move(data));
		}
	}

	template<typename T>
	bool DoublyLinkedList<T>::Delete(const T& data)
	{
		/*if (mRoot == nullptr)
		{
			return false;
		}
		unsigned int index = 0;
		std::shared_ptr<Node<T>> tempNode = mRoot;
		while (index < mCount)
		{
			if (*(tempNode->Data) == data)
			{
				if (index == 0 && mCount == 1)
				{
					mRoot.reset();
				}
				else if (index == 0)
				{
					mRoot = mRoot->Next;
				}
				else if (index < mCount - 1)
				{
					std::shared_ptr<Node<T>> lockPrevNode = tempNode->Previous.lock();
					lockPrevNode->Next = tempNode->Next;
					tempNode->Next->Previous = lockPrevNode;
				}
				else
				{
					std::shared_ptr<Node<T>> lockPrevNode = tempNode->Previous.lock();
					lockPrevNode->Next = nullptr;
				}
				mCount--;
				return true;
			}
			tempNode = tempNode->Next;
			index++;
		}*/
		int index = Find(data);
		if (index > -1)
		{
			std::shared_ptr<Node<T>> tempNode = (*this)[index];
//			std::shared_ptr<Node<T>> lockedPrevNode = tempNode->Previous.lock();
			std::shared_ptr<Node<T>> nextNode = tempNode->Next;
			tempNode->Previous.lock()->Next = nextNode;
			nextNode->Previous = tempNode->Previous;
			mCount--;
			return true;
		}
		return false;
	}

	template<typename T>
	int DoublyLinkedList<T>::Find(const T& data) const
	{
		if (mRoot == nullptr)
		{
			return -1;
		}
		std::shared_ptr<Node<T>> tempNode = mRoot;
		unsigned int index = 0;
		while (index < mCount)
		{
			if (*(tempNode->Data) == data)
			{
				return index;
			}
			tempNode = tempNode->Next;
			index++;
		}
		return -1;
	}

	template<typename T>
	bool DoublyLinkedList<T>::Search(const T& data) const
	{
		if (Find(data) > -1)
		{
			return true;
		}
		return false;
	}

	template<typename T>
	unsigned int DoublyLinkedList<T>::GetLength() const
	{
		return mCount;
	}

	template<typename T>
	std::shared_ptr<Node<T>> DoublyLinkedList<T>::operator[](unsigned int index) const
	{
		if (mCount > index)
		{
			std::shared_ptr<Node<T>> tempNode = mRoot;
			while (index > 0)
			{
				tempNode = tempNode->Next;
				index--;
			}
			return tempNode;
		}
		return nullptr;
	}
}
#include "Lab5.h"
#include <algorithm>

namespace lab5
{
	int Sum(const std::vector<int>& v)
	{
		int sum = 0;
		for (std::vector<int>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
		{
			sum += *iter;
		}
		return sum;
	}

	int Min(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}
		int min = v[0];
		for (std::vector<int>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
		{
			if (*iter < min)
			{
				min = *iter;
			}
		}
		return min; // implement Min
	}

	int Max(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}
		int max = v[0];
		for (std::vector<int>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
		{
			if (*iter > max)
			{
				max = *iter;
			}
		}
		return max; // implement Max
	}

	float Average(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}
		int sum = Sum(v);
		return (float)sum / (float)v.size(); // implement Average
	}

	int NumberWithMaxOccurrence(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}
		std::vector<int> s = v;
		std::sort(s.begin(), s.end());
		int maxOccurrence = s[0];
		int maxRun = 1;
		int currentRun = 1;
		int previous = s[0];
		for (std::vector<int>::const_iterator iter = s.begin() + 1; iter != s.end(); ++iter)
		{
			if (*iter == previous)
			{
				currentRun++;
				if (currentRun > maxRun)
				{
					maxRun = currentRun;
					maxOccurrence = *iter;
				}
			}
			else
			{
				previous = *iter;
				currentRun = 1;
			}
		}
		return maxOccurrence; // implement NumberWithMaxOccurrence
	}
}
#include "Vector.h"

#include <iostream>

using namespace lab4;

int main()
{
	Vector a(4, -5, 6);
	Vector b(7, 2, -6);

	Vector sum = a + b;
	Vector difference = a - b;
	float dotProduct = a * b;
	Vector scalerProduct = a * 2;
	Vector scalerProduct2 = 2 * a;

	std::cout << difference[0] << std::endl;
	std::cout << difference[1] << std::endl;
	std::cout << difference[2] << std::endl;
	std::cout << sum[0] << std::endl;
	std::cout << sum[1] << std::endl;
	std::cout << sum[2] << std::endl;
	std::cout << scalerProduct[0] << std::endl;
	std::cout << scalerProduct[1] << std::endl;
	std::cout << scalerProduct[2] << std::endl;
	std::cout << scalerProduct2[0] << std::endl;
	std::cout << scalerProduct2[1] << std::endl;
	std::cout << scalerProduct2[2] << std::endl;
	std::cout << scalerProduct2[3] << std::endl;
	std::cout << scalerProduct2[4] << std::endl;
	std::cout << scalerProduct2[5] << std::endl;

	return 0;
}
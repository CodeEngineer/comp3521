#include "Vector.h"

namespace lab4
{
	Vector::Vector()
	{
		mX = 0;
		mY = 0;
		mZ = 0;
	}

	Vector::Vector(float x, float y, float z)
	{
		mX = x;
		mY = y;
		mZ = z;
	}

	Vector::Vector(const Vector& other)
		: mX(other.mX)
		, mY(other.mY)
		, mZ(other.mZ)
	{
	}

	Vector::~Vector()
	{
	}

	Vector Vector::operator+(const Vector& other) const
	{
		// implement vector addition here
		Vector sum;

		sum.mX = mX + other.mX;
		sum.mY = mY + other.mY;
		sum.mZ = mZ + other.mZ;

		return sum;
	}

	Vector Vector::operator-(const Vector& other) const
	{
		// implement vector subtraction here
		Vector difference;

		difference.mX = mX - other.mX;
		difference.mY = mY - other.mY;
		difference.mZ = mZ - other.mZ;

		return difference;
	}

	float Vector::operator*(const Vector& other) const
	{

		return mX * other.mX + mY * other.mY + mZ * other.mZ; // implement dot product here
	}

	Vector Vector::operator*(float operand) const
	{
		// implement scaler multiplication of vector
		Vector scaler;

		scaler.mX = mX * operand;
		scaler.mY = mY * operand;
		scaler.mZ = mZ * operand;

		return scaler;
	}

	float Vector::operator[](unsigned int index) const
	{
		switch (index)
		{
		case 0:
			return mX;
		case 1:
			return mY;
		case 2:
			return mZ;
		default:
			return mZ;
		}
	}

	float Vector::GetX() const
	{
		return mX; // return x component
	}

	float Vector::GetY() const
	{
		return mY; // return y component
	}

	float Vector::GetZ() const
	{
		return mZ; // return z component
	}

	Vector operator*(float operand, const Vector& v)
	{
		// implement scaler multiplication of vector
		Vector scalar;

		scalar.mX = v.mX * operand;
		scalar.mY = v.mY * operand;
		scalar.mZ = v.mZ * operand;

		return scalar;
	}
}
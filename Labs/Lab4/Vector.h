#pragma once
#include "iostream"

#define VECTOR_SIZE 3

using namespace std;

namespace lab4
{
	class Vector
	{
	public:
		Vector();
		Vector(float x, float y, float z);
		virtual ~Vector();

		Vector operator+(const Vector& other) const;
		Vector operator-(const Vector& other) const;
		float operator*(const Vector& other) const;
		Vector operator*(float operand) const;
		float operator[](unsigned int index) const;

		float GetX() const;
		float GetY() const;
		float GetZ() const;

		Vector(const Vector& other);

		friend Vector operator*(float operand, const Vector& v);

		friend Vector operator*(const Vector& lhs, int scalar);
		friend void operator<<(std::ostream& os, const Vector& rhs);


	private:
		// private variables here
		float mX;
		float mY;
		float mZ;
	};
}
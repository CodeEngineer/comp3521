#include "Lab2.h"

using namespace lab2;

int main(int argc, char** argv)
{
	// run using command line Lab2.exe < input.txt or input2.txt
	//lab2::PrintIntegers(std::cin, std::cout);
	lab2::PrintMaxFloat(std::cin, std::cout);

	return 0;
}
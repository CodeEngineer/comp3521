#include "Lab2.h"

#include <string>
#include <limits>
#include <iomanip>

using namespace std;

namespace lab2
{
	void PrintIntegers(istream& in, ostream& out)
	{
		int number;
		int widthOct = 12;
		int widthDec = 11;
		int widthHex = 9;
		bool titlePrinted = false;

		while (!in.eof())
		{
			if (in >> number)
			{
				if (!titlePrinted)
				{
					out << setw(widthOct) << "oct";
					out << setw(widthDec) << "dec";
					out << setw(widthHex) << "hex" << endl;
					out << setfill('-') << setw(widthOct + 1) << " ";
					out << setfill('-') << setw(widthDec) << " ";
					out << setfill('-') << setw(widthHex - 1) << "" << endl;
					titlePrinted = true;
				}
				out << oct << setfill(' ') << setw(widthOct) << number;
				out << dec << setw(widthDec) << number;
				out << uppercase << hex << setw(widthHex) << number << endl;
			}
			if (in.fail())
			{
				in.clear();
				in.ignore();
			}
		}
	}

	void PrintMaxFloat(istream& in, ostream& out)
	{
		float number;
		float* max = new float(numeric_limits<float>::min());
		int signWidth = 6;
		int numberWidth = 15;

		while (!in.eof())
		{
			if (in >> number)
			{
				if (number > *max)
				{
					*max = number;
				}
				out << "     ";
				out << setw(numberWidth) << setprecision(3) << fixed << showpos << internal << number << endl;
			}
			else
			{
				in.clear();
				in.ignore();
			}
		}
		out << "max: ";
		out << setw(numberWidth) << setprecision(3) << fixed << showpos << internal << *max << endl;
		delete max;
	}
}
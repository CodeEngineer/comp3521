#pragma once

namespace lab6
{
	template<class T, size_t N>
	class FixedVector
	{
	public:
		FixedVector();
		~FixedVector() = default;
		const T& Get(unsigned int index) const;
		T& operator[](unsigned int index);
		int GetIndex(const T& t) const;
		size_t GetSize() const;
		size_t GetCapacity() const;
		bool Add(const T& t);
		bool Remove(const T& t);

	private:
		// private variable here
		size_t mSize;
		T mArray[N];
	};

	template<class T, size_t N>
	FixedVector<T, N>::FixedVector()
		: mSize(0)
	{
	}

	template<class T, size_t N>
	const T& FixedVector<T, N>::Get(unsigned int index) const
	{
		return mArray[index];
	}

	template<class T, size_t N>
	T& FixedVector<T, N>::operator[](unsigned int index)
	{
		return mArray[index];
	}

	template<class T, size_t N>
	int FixedVector<T, N>::GetIndex(const T& t) const
	{
		for (size_t i = 0; i < mSize; i++)
		{
			if (mArray[i] == t)
			{
				return i;
			}
		}
		return -1;
	}

	template<class T, size_t N>
	size_t FixedVector<T, N>::GetSize() const
	{
		return mSize;
	}

	template<class T, size_t N>
	size_t FixedVector<T, N>::GetCapacity() const
	{
		return N;
	}

	template<class T, size_t N>
	bool FixedVector<T, N>::Add(const T& t)
	{
		if (mSize < N)
		{
			mArray[mSize++] = t;
			return true;
		}
		return false;
	}

	template<class T, size_t N>
	bool FixedVector<T, N>::Remove(const T& t)
	{
		int index = GetIndex(t);
		if (GetIndex(t) > -1)
		{
			for (size_t i = index; i < mSize - 1; i++)
			{
				mArray[i] = mArray[i + 1];
			}
			mSize--;
			return true;
		}
		return false;
	}

}
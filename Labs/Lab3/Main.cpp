#include "TimeSheet.h"
#include <string>
#include <iostream>

using namespace std;
using namespace lab3;
int main()
{
	const char* name = "one";
	TimeSheet employeeOne = TimeSheet(name, 5);
	employeeOne.AddTime(5);
	cout << employeeOne.GetTotalTime() << endl;
	cout << employeeOne.GetName() << endl;
	cout << employeeOne.GetAverageTime() << endl;
	return 0;
}
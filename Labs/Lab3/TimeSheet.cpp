#include "TimeSheet.h"

#define DAILY_HOUR_LIMIT 10

namespace lab3
{
	TimeSheet::TimeSheet(const char* name, int maxEntries)
		: maxEntries(maxEntries)
	{
		employeeName = name;
		numDays = int(0);
		totalHours = float(0);
	}

	TimeSheet::TimeSheet(const TimeSheet& other)
		: maxEntries(other.maxEntries)
	{
		employeeName = other.employeeName;
		numDays = other.GetNumDays();
		totalHours = other.GetTotalTime();
	}

	TimeSheet::~TimeSheet()
	{
		employeeName = "";
		name_length = 0;
		maxEntries = 0;
		numDays = 0;
		totalHours = 0;
	}

	int TimeSheet::GetNumDays() const
	{
		return numDays;
	}

	void TimeSheet::AddTime(float timeInHours)
	{
		if (timeInHours <= DAILY_HOUR_LIMIT && numDays < maxEntries && timeInHours > 0)
		{
			numDays++;
			totalHours += timeInHours;
		}
	}

	float TimeSheet::GetTotalTime() const
	{
		return totalHours;
	}

	float TimeSheet::GetAverageTime() const
	{
		if (numDays > 0)
		{
			return totalHours / (float)numDays;
		}
		else
		{
			return 0;
		}
	}

	const std::string& TimeSheet::GetName() const
	{
		std::string *a = new std::string(employeeName);
		return *a;
	}
}
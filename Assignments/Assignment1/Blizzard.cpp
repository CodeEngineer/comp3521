#include "Blizzard.h"

namespace assignment1
{
	Blizzard::Blizzard(const char* name, unsigned int scoops)
		: IceCream(name, scoops)
	{
		mCost = COST_OF_TOPPING + IceCream::GetCost();
	}



	Blizzard::~Blizzard()
	{
	}

	unsigned int Blizzard::GetCost() const
	{
		return mCost; // return cost here
	}
}
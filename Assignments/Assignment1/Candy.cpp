#include "Candy.h"
#include <cmath>

namespace assignment1
{
	Candy::Candy(const char* name, float weight)
		: Item(name)
	{
		mWeight = weight;
		mCost = (unsigned int)((float)PRICE_PER_LBS * weight);
	}

	Candy::~Candy()
	{
	}

	unsigned int Candy::GetCost() const
	{
		return mCost; // return cost here
	}
}
#include "Donut.h"

#include <cmath>

namespace assignment1
{
	Donut::Donut(const char* name, unsigned int count)
		: Item(name)
	{
		mCount = count;
		mCost = (unsigned int)round((double)count * (double)PRICE_PER_DOZEN / (double)DOZEN);
	}

	unsigned int Donut::GetCost() const
	{
		return mCost; // return cost here
	}
}
#pragma once

#include "Item.h"

#define PRICE_PER_LBS 68

namespace assignment1
{
	class Candy : public Item
	{
	public:
		Candy(const char* name, float weight);

		unsigned int GetCost() const;
		virtual ~Candy();
	private:
		// private variables here
		std::string mName;
		float mWeight;
		unsigned int mCost;
	};
}
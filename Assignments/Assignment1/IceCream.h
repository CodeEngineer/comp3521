#pragma once

#include "Item.h"

#define PRICE_PER_OUNCE 120
#define OUNCE_PER_SCOOP 2

namespace assignment1
{
	class Cone;

	class IceCream : public Item
	{
	public:
		IceCream(const char* name, unsigned int numScoops);
		IceCream(const IceCream& other);
		unsigned int GetCost() const;
		void AddCone();
		virtual ~IceCream();
	protected:
		unsigned int mCost;
	private:
		// private variables here
		unsigned int mNumScoops;
		Cone* mCone;
	};
}
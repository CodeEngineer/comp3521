#pragma once

#include "IceCream.h"

#define COST_OF_TOPPING 120

namespace assignment1
{
	class Blizzard : public IceCream
	{
	public:
		Blizzard(const char* name, unsigned int numScoops);

		unsigned int GetCost() const;
		virtual ~Blizzard();
	};
}
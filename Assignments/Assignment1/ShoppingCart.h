#pragma once

#define CART_SIZE 10

namespace assignment1
{
	class Item;

	class ShoppingCart
	{
	public:
		ShoppingCart();

		bool AddItem(const Item* item);
		bool RemoveItem(unsigned int dex);

		const Item* GetItem(unsigned int index) const;
		const Item* operator[](unsigned int index) const;

		float GetTotal() const;
		virtual ~ShoppingCart();
	private:
		ShoppingCart(const ShoppingCart& other) {}

		// private variables here
		unsigned int mCount;
		const Item** items;
	};
}
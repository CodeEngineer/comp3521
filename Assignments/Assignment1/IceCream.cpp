#include "IceCream.h"
#include "Cone.h"

#include <cmath>

namespace assignment1
{
	IceCream::IceCream(const char* name, unsigned int scoops)
		: Item(name)
		, mCone(NULL)
	{
		mNumScoops = scoops;
		mCost = mNumScoops * OUNCE_PER_SCOOP * PRICE_PER_OUNCE;
	}

	IceCream::IceCream(const IceCream& other)
		: Item(other)
	{
		if (other.mCone != NULL)
		{
			mCone = new Cone(*other.mCone);
		}
		mCost = other.GetCost();
		mNumScoops = other.mNumScoops;
	}

	unsigned int IceCream::GetCost() const
	{
		return mCost; // return cost here
	}

	void IceCream::AddCone()
	{
		if (mCone == NULL)
		{
			mCone = new Cone();
			mCost += mCone->GetCost();
		}
	}

	IceCream::~IceCream()
	{
		delete mCone;
	}
}
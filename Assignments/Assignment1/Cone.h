#pragma once

#define PRICE_PER_CONE 10

namespace assignment1
{
	class Cone
	{
	public:
		Cone();
		Cone(const Cone& other);

		virtual ~Cone();
		unsigned int GetCost() const;
	private:
		unsigned int mCost;
	};
}
#include "Cone.h"

namespace assignment1
{
	Cone::Cone()
	{
		mCost = PRICE_PER_CONE;
	}

	Cone::Cone(const Cone& other)
	{
		mCost = other.GetCost();
	}

	Cone::~Cone()
	{
	}

	unsigned int Cone::GetCost() const
	{
		return mCost; // return cost here
	}
}
#include "ShoppingCart.h"
#include "Item.h"

namespace assignment1
{
	ShoppingCart::ShoppingCart()
	{
		mCount = 0;
		items = new const Item*[CART_SIZE];
		for (int i = 0; i < CART_SIZE; i++)
		{
			items[i] = NULL;
		}
	}

	bool ShoppingCart::AddItem(const Item* item)
	{
		if (mCount < CART_SIZE && item != NULL)
		{
			items[mCount] = item;
			mCount++;
			return true;
		}
		return false; // return true if success. Else return false
	}

	bool ShoppingCart::RemoveItem(unsigned int index)
	{
		if (mCount > 0 && index < mCount)
		{
			delete items[index];
			for (unsigned int i = index; i < mCount - 1; i++)
			{
				items[i] = items[i + 1];
			}
			items[mCount] = NULL;
			mCount--;
			return true;
		}
		return false; // return true if success. Else return false
	}

	const Item* ShoppingCart::GetItem(unsigned int index) const
	{
		if (index < mCount)
		{
			return items[index];
		}
		return NULL;
	}

	const Item* ShoppingCart::operator[](unsigned int index) const
	{
		if (index < mCount)
		{
			return items[index];
		}
		return NULL;
	}

	float ShoppingCart::GetTotal() const
	{
		float total = 0;
		for (unsigned int i = 0; i < mCount; i++)
		{
			total += items[i]->GetCost();
		}
		return total / 100; // return total cost in dollars
	}

	ShoppingCart::~ShoppingCart()
	{
		for (unsigned int i = 0; i < mCount; i++)
		{
			if (items[i] != NULL)
			{
				delete items[i];
			}
		}
		delete[] items;
	}
}
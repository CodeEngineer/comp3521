#include <iostream>

#include "Blizzard.h"
#include "Candy.h"
#include "Donut.h"
#include "IceCream.h"
#include "Item.h"
#include "ShoppingCart.h"


#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

using namespace assignment1;

int main()
{
	{
		ShoppingCart cart;

		Blizzard* blizzard = new Blizzard("Smarties Blizzard", 3);
		Item* candy = new Candy("Gummy Bears", 2.5f);
		Item* donut = new Donut("Canadian Maple", 4);
		IceCream* icecream = new IceCream("Vanilla", 2);
		
		icecream->AddCone();
		IceCream& iceCreamRef = *icecream;
		IceCream* icecreamCopy = new IceCream(iceCreamRef);
	
		cart.AddItem(blizzard);
		cart.AddItem(candy);
		cart.AddItem(donut);
		cart.AddItem(icecream);
		cart.AddItem(icecreamCopy);

		std::cout << cart.GetTotal() << std::endl; // should print 18

		cart.RemoveItem(0);
		cart.RemoveItem(0);
		cart.RemoveItem(0);
		cart.RemoveItem(0);
		cart.RemoveItem(0);
		cart.RemoveItem(0);
	}
	_CrtDumpMemoryLeaks();
	/*<---- Breakpoint here & check console output.
	Function call will print out any objects
	still remaining on the heap.
	*/
	return 0;
}
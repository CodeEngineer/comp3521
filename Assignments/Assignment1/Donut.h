#pragma once

#include "Item.h"

#define DOZEN 12
#define PRICE_PER_DOZEN 899

namespace assignment1
{
	class Donut : public Item
	{
	public:
		Donut(const char* name, unsigned int count);

		unsigned int GetCost() const;

	private:
		// private variables here
		unsigned int mCount;
		unsigned int mCost;
	};
}
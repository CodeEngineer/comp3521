#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;

namespace assignment2
{
	template <typename K, class V>
	map<K, V> ConvertVectorsToMap(const vector<K>& keys, const vector<V>& data)
	{
		// implement ConvertVectorsToMap here
		map<K, V> convertedMap;
		class vector<V>::const_iterator iter2 = data.begin();
		for (typename vector<K>::const_iterator iter = keys.begin();
			 iter != keys.end() && iter2 != data.end(); 
			 ++iter, ++iter2)
		{
			convertedMap.insert(pair<K, V>(*iter, *iter2));
		}
		return convertedMap;
	}

	template <typename K, class V>
	vector<K> GetKeys(const map<K, V>& m)
	{
		// imlement GetKeys here
		vector<K> keys;
		for (typename map<K, V>::const_iterator iter = m.begin(); iter != m.end(); ++iter)
		{
			keys.push_back(iter->first);
		}
		return keys;
	}

	template <typename K, class V>
	vector<V> GetValues(const map<K, V>& m)
	{
		// implement GetValues here
		vector<V> values;
		for (typename map<K, V>::const_iterator iter = m.begin(); iter != m.end(); ++iter)
		{
			values.push_back(iter->second);
		}
		return values;
	}

	template <typename T>
	vector<T> Reverse(const vector<T>& v)
	{
		// implement Reverse here
		vector<T> reversed;
		for (typename vector<T>::const_reverse_iterator iter = v.rbegin(); iter != v.rend(); ++iter)
		{
			reversed.push_back(*iter);
		}
		return reversed;
	}
}

template <typename T>
vector<T> operator+(const vector<T>& v1, const vector<T>& v2)
{
	// implement operator +' here
	vector<T> combined;
	for (typename vector<T>::const_iterator iter = v1.begin(); iter != v1.end(); ++iter)
	{
		combined.push_back(*iter);
	}
	for (typename vector<T>::const_iterator iter = v2.begin(); iter != v2.end(); ++iter)
	{
		if (std::find(combined.begin(), combined.end(), *iter) == combined.end())
		{
			combined.push_back(*iter);
		}
	}
	return combined;
}

template <typename K, class V>
map<K, V> operator+(const map<K, V>& m1, const map<K, V>& m2)
{
	// implement operator '+' here
	map<K, V> combined;
	for (typename map<K, V>::const_iterator iter = m1.begin(); iter != m1.end(); ++iter)
	{
		combined.insert(pair<K, V>(*iter));
	}
	for (typename map<K, V>::const_iterator iter = m2.begin(); iter != m2.end(); ++iter)
	{
		combined.insert(pair<K, V>(*iter));
		//typename map<K, V>::iterator it = combined.find(iter->first);
		//if (it != combined.end()) {
		//	it->second = iter->second;
		//}
		//else
		//{
		//	combined.insert(pair<K, V>(*iter));
		//}
	}

	return combined;
}

template<typename T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
	// implement operator '<<' here
	for (typename vector<T>::const_iterator iter = v.begin(); iter != v.end(); ++iter)
	{
		if (iter == v.end() - 1)
		{
			os << *iter;
		}
		else
		{
			os << *iter << ", ";
		}
	}
	return os;
}

template <typename K, class V>
ostream& operator<<(ostream& os, const map<K, V>& m)
{
	// implement operator '<<' here
	for (typename map<K, V>::const_iterator iter = m.begin(); iter != m.end(); ++iter)
	{
		os << "{ " << iter->first << ", " << iter->second << " }" << endl;
	}
	return os;
}


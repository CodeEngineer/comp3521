#include "Assignment2.h"
#include "MidtermTwoQSeven.h"

using namespace std;

int main()
{
	vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);

	vector<char> v2;
	v2.push_back('a');
	v2.push_back('b');
	v2.push_back('c');

	vector<int> v3;
	v3.push_back(3);
	v3.push_back(5);
	v3.push_back(6);

	map<char, int> m1;
	m1['a'] = 1;
	m1['b'] = 2;
	m1['c'] = 3;

	map<char, int> m2;
	m2['c'] = 4;
	m2['e'] = 5;
	m2['f'] = 6;

	map<int, char> m = assignment2::ConvertVectorsToMap(v1, v2);
	vector<char> keys = assignment2::GetKeys(m1);
	vector<int> values = assignment2::GetValues(m1);
	vector<int> reversedV1 = assignment2::Reverse(v1);
	vector<int> combinedVector = v1 + v3;
	map<char, int> combinedMap = m1 + m2;

	cout << m << endl;

	map<int, int> map;
	map[12] = 5;
	map[20];
	map[0] = 1;

	vector<int> v4(7, 10);
	vector<int> v5(6);
	copy(v4.begin() + 2, v4.end() - 2, v5.begin());

	vector<int> v6(2);

	v6.push_back(1);
	v6.push_back(2);
	v6.push_back(3);
	v6.push_back(4);

	std::map<int, char> map1;

	map1.insert(std::pair<int, char>(1, 'a'));
	map1.insert(std::pair<int, char>(2, 'b'));
	map1.insert(std::pair<int, char>(5, 'c'));
	map1.insert(std::pair<int, char>(11, 'd'));

	assignment2::MyClass<bool, int> a(true, 10);
	assignment2::MyClass<int, char> b(2, 'a');
	assignment2::MyClass<char, char> c('a', 'b');

	vector<int> v7;
	v7.push_back(5);
	v7.push_back(6);
	v7.push_back(4);
	v7.push_back(7);

	v7.resize(1);

	for (vector<int>::iterator iter = v7.begin(); iter != v7.end(); ++iter)
	{
		cout << *iter << endl;
	}

	a.DoSomething();
	b.DoSomething();
	c.DoSomething();

	std::map<int, char>::iterator iter = map1.begin();

	cout << iter->first << endl;

	system("pause");

	return 0;
}
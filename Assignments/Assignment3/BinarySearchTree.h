#pragma once

#include <memory>
#include <vector>
#include <iostream>
#include <stack>

namespace assignment3
{
	template<typename T>
	class TreeNode;

	template<typename T>
	class BinarySearchTree
	{
	public:
		BinarySearchTree();
		~BinarySearchTree();
		void Insert(std::unique_ptr<T> data);
		bool Search(const T& data);
		bool Delete(const T& data);
		const std::weak_ptr<TreeNode<T>> GetRootNode() const;
		static std::vector<T> TraverseInOrder(const std::shared_ptr<TreeNode<T>> startNode);

	private:
		std::shared_ptr<TreeNode<T>> mRoot;
		// any other private members of methods can go here.
	};

	template<typename T>
	BinarySearchTree<T>::BinarySearchTree()
	{
	}

	template<typename T>
	BinarySearchTree<T>::~BinarySearchTree()
	{
	}

	template<typename T>
	void BinarySearchTree<T>::Insert(std::unique_ptr<T> data)
	{
		if (mRoot == nullptr)
		{
			std::shared_ptr<TreeNode<T>> node = std::make_shared<TreeNode<T>>(std::move(data));
			mRoot = node;
		}
		else
		{
			std::shared_ptr<TreeNode<T>> tempNode = mRoot;
			while (tempNode->Left != nullptr && *tempNode->Data >= *data
				|| tempNode->Right != nullptr && *tempNode->Data <= *data)
			{
				if (*tempNode->Data >= *data)
				{
					tempNode = tempNode->Left;
					continue;
				}
				else if (*(tempNode->Data) <= *data)
				{
					tempNode = tempNode->Right;
					continue;
				}
			}
			if (*tempNode->Data >= *data)
			{
				std::shared_ptr<TreeNode<T>> node = std::make_shared<TreeNode<T>>(tempNode, std::move(data));
				tempNode->Left = node;
			}
			else if (*(tempNode->Data) <= *data)
			{
				std::shared_ptr<TreeNode<T>> node = std::make_shared<TreeNode<T>>(tempNode, std::move(data));
				tempNode->Right = node;
			}
		}
	}

	template<typename T>
	bool BinarySearchTree<T>::Search(const T& data)
	{
		if (mRoot == nullptr)
		{
			return false;
		}
		std::shared_ptr<TreeNode<T>> tempNode = mRoot;
		while (tempNode->Left != nullptr && *(tempNode->Data) >= data
			|| tempNode->Right != nullptr && *(tempNode->Data) <= data)
		{
			if (*tempNode->Data > data)
			{
				tempNode = tempNode->Left;
			}
			else if (*(tempNode->Data) < data)
			{
				tempNode = tempNode->Right;
			}
			else if (*tempNode->Data == data)
			{
				return true;
			}
		}
		if (*(tempNode->Data) == data)
		{
			return true;
		}
		return false;
	}

	template<typename T>
	bool BinarySearchTree<T>::Delete(const T& data)
	{
		bool left = false;
		bool right = false;
		if (mRoot == nullptr)
		{
			return false;
		}
		std::shared_ptr<TreeNode<T>> tempNode = mRoot;
		while (tempNode->Left != nullptr && *tempNode->Data >= data
			|| tempNode->Right != nullptr && *tempNode->Data <= data)
		{
			if (*(tempNode->Data) > data)
			{
				tempNode = tempNode->Left;
				left = true;
				right = false;
			}
			else if (*tempNode->Data < data)
			{
				tempNode = tempNode->Right;
				left = false;
				right = true;
			}
			else if (*tempNode->Data == data)
			{
				break;
			}
		}
		if (*tempNode->Data == data)
		{
			if (tempNode->Left == nullptr && tempNode->Right == nullptr)
			{
				if (!left && !right)
				{
					mRoot.reset();
				}
				else
				{
					std::shared_ptr<TreeNode<T>> tempPrevNode = tempNode->Parent.lock();
					if (left)
					{
						tempPrevNode->Left.reset();
					}
					else
					{
						tempPrevNode->Right.reset();
					}
				}
			}
			else if (tempNode->Left == nullptr)
			{
				if (!left && !right)
				{
					mRoot = mRoot->Right;
				}
				else
				{
					std::shared_ptr<TreeNode<T>> tempPrevNode = tempNode->Parent.lock();
					if (left)
					{
						tempPrevNode->Left = tempNode->Right;
						tempNode->Right->Parent = tempPrevNode;
					}
					else
					{
						tempPrevNode->Right = tempNode->Right;
						tempNode->Right->Parent = tempPrevNode;
					}
				}
			}
			else if (tempNode->Right == nullptr)
			{
				if (!left && !right)
				{
					mRoot = mRoot->Left;
				}
				else
				{
					std::shared_ptr<TreeNode<T>> tempPrevNode = tempNode->Parent.lock();
					if (left)
					{
						tempPrevNode->Left = tempNode->Left;
						tempNode->Left->Parent = tempPrevNode;
					}
					else
					{
						tempPrevNode->Right = tempNode->Left;
						tempNode->Left->Parent = tempPrevNode;
					}
				}
			}
			else
			{
				std::shared_ptr<TreeNode<T>> minRightNode = tempNode->Right;
				while (minRightNode->Left != nullptr)
				{
					minRightNode = minRightNode->Left;
				}
				tempNode->Data = std::move(minRightNode->Data);
				minRightNode->Parent.lock()->Left = nullptr;
			}
			return true;
		}
		return false;
	}

	template<typename T>
	const std::weak_ptr<TreeNode<T>> BinarySearchTree<T>::GetRootNode() const
	{
		return mRoot;
	}

	template<typename T>
	std::vector<T> BinarySearchTree<T>::TraverseInOrder(
		const std::shared_ptr<TreeNode<T>> startNode)
	{
		std::vector<T> v;
		if (startNode == nullptr)
		{
			return v;
		}
		std::shared_ptr<TreeNode<T>> currNode(startNode);
		std::stack<std::shared_ptr<TreeNode<T>>> stack;
		while (!stack.empty() || currNode != nullptr)
		{
			if (currNode != nullptr)
			{
				stack.push(currNode);
				currNode = currNode->Left;
			}
			else
			{
				currNode = stack.top();
				stack.pop();
				v.push_back(*currNode->Data);
				currNode = currNode->Right;
			}
		}
		return v;
	}
}